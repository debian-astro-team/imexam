Source: imexam
Maintainer: Debian Astronomy Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: science
Priority: optional
Build-Depends: cython3,
               debhelper-compat (= 13),
               dh-python,
               ipython3,
               libxpa-dev,
               python3-all-dev,
               python3-astropy,
               python3-ginga,
               python3-matplotlib,
               python3-photutils,
               python3-pytest,
               python3-scipy,
               python3-setuptools,
               python3-setuptools-scm,
               python3-sphinx (>= 2.0),
               python3-sphinx-astropy,
               python3-sphinx-automodapi
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/debian-astro-team/imexam
Vcs-Git: https://salsa.debian.org/debian-astro-team/imexam.git
Homepage: https://imexam.readthedocs.io
Rules-Requires-Root: no

Package: python3-imexam
Architecture: any
Section: python
Depends: python3-matplotlib,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Recommends: ipython3,
            python3-ginga | saods9,
            python3-ginga | xpa-tools,
            python3-scipy
Suggests: python-imexam-doc
Description: Simple interactive astronomical image examination and plotting
 Imexam is an affiliated package of AstroPy. It was designed to be a
 lightweight library which enables users to explore data using common
 methods which are consistent across viewers. It can be used from a
 command line interface, through a Jupyter notebook or through a
 Jupyter console. It can be used with multiple viewers, such as DS9 or
 Ginga, or without a viewer as a simple library to make plots and grab
 quick photometry information.

Package: python-imexam-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Description: Simple interactive astronomical image examination and plotting (Doc)
 Imexam is an affiliated package of AstroPy. It was designed to be a
 lightweight library which enables users to explore data using common
 methods which are consistent across viewers. It can be used from a
 command line interface, through a Jupyter notebook or through a
 Jupyter console. It can be used with multiple viewers, such as DS9 or
 Ginga, or without a viewer as a simple library to make plots and grab
 quick photometry information.
 .
 This package contains the API documentation.
